package com.example.customealertbox;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Context mContext;
    Button mBtnCustomDialogue;
    Button mBtnRatingBar;
//    Button btndialoYes,btndialoNo;
     private Button mButtonOk;
     private Button mButtonCancel;
     private Button mSubmitReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = MainActivity.this;

        bindViews();
     }

    public void bindViews() {
        mBtnCustomDialogue = (Button) findViewById(R.id.btnCustomDialogue);
        mBtnRatingBar = (Button) findViewById(R.id.btnRatingBar);

        mBtnCustomDialogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opencustomdialog();
            }
        });
        mBtnRatingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRatingBar();
            }

        });
    }

    private void opencustomdialog() {

        LayoutInflater vi;
        vi = LayoutInflater.from(mContext);
        View dialoglayout = vi.inflate(R.layout.custom_dialogue_view_1, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(dialoglayout);
        builder.setCancelable(false);
        builder.setMessage("CUSTOM ALERT");

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        mButtonOk = (Button) dialoglayout.findViewById(R.id.buttonOk);
        mButtonCancel = (Button)dialoglayout.findViewById(R.id.buttonCancel) ;
        mButtonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "You pressed Cancel", Toast.LENGTH_SHORT).show();
            }
        });

    }
    //bsjbjksanxjkasnxjk
    private void openRatingBar() {
        LayoutInflater vi1;
        vi1 = LayoutInflater.from(mContext);
        View  ratingdialogue = vi1.inflate(R.layout.custon_ratin_bar,null);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(ratingdialogue);
        builder.setCancelable(true);

        final  AlertDialog alertDialog = builder.create();
        alertDialog.show();


        mSubmitReview  = (Button) ratingdialogue.findViewById(R.id.btnSubmitReview);
        final RatingBar ratingBar = ratingdialogue.findViewById(R.id.rating);
        mSubmitReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rating = "Rating is " + ratingBar.getRating();
                Toast.makeText(mContext, rating, Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });


    }
}
